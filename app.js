"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var discord = require("discord.js");
var cron = require("node-schedule");
var help_1 = require("./commands/help");
var quote_1 = require("./commands/quote");
var self_roles_1 = require("./commands/self-roles");
var moderation_1 = require("./commands/moderation");
var prompt_pins_1 = require("./commands/prompt-pins");
var derpi_1 = require("./commands/derpi");
var article_1 = require("./commands/article");
// Consts
var bot = new discord.Client();
// Prefix
var _pre = '!';
var _pre_admin = '&';
// Schedules
var schedules = {
    t2: "0 0,12 * * *",
    t3a: "0 0,8,16 * * *",
    t3b: "0 4,12,20 * * *",
    t4: "0 0,6,12,18 * * *",
    t6: "0 0,4,8,12,16,20 * * *",
    t8: "0 0,3,6,9,12,15,18,21 * * *",
    tt: "* * * * *",
};
// Bot proper
bot.on('ready', function () {
    console.log('Bot is ready!');
});
// Schedule
var q = cron.scheduleJob(schedules.t4, function () {
    var chan = bot.channels.get(process.env.OFFTOPIC_CHANNEL || '');
    quote_1.sendQuote(chan);
});
var q2 = cron.scheduleJob(schedules.t3a, function () {
    var chan = bot.channels.get(process.env.PICS_CHANNEL || '');
    derpi_1.sendDerpi(chan, 'rarijack', '#ff9e18');
});
var q3 = cron.scheduleJob(schedules.t3b, function () {
    var chan = bot.channels.get(process.env.PICS_CHANNEL || '');
    derpi_1.sendDerpi(chan, 'startrix', '#b454ff');
});
// Handle commands
bot.on('message', function (msg) {
    var cont = msg.content;
    if (cont.startsWith(_pre)) {
        var comm = cont.replace(_pre, '');
        // Help command
        if (comm === 'h' || comm === 'help') {
            help_1.help(msg);
        }
        // Quote command
        else if (comm === 'q' || comm === 'quote') {
            quote_1.quote(msg);
        }
        // Role management – promptme
        else if (comm.startsWith('promptme')) {
            var m = comm.replace('promptme', '').trim();
            if (m === 'on')
                self_roles_1.promptmeOn(msg);
            else if (m === 'off')
                self_roles_1.promptmeOff(msg);
            // Role management – discussion club
        }
        else if (comm.startsWith('discussion-club')) {
            var m = comm.replace('discussion-club', '').trim();
            if (m === 'on')
                self_roles_1.discussionClubOn(msg);
            else if (m === 'off')
                self_roles_1.discussionClubOff(msg);
        }
        // Prompt submission pinning
        else if (comm.startsWith('sub')) {
            prompt_pins_1.promptPin(msg);
        }
        // Helpers' pin
        else if (comm === 'pin') {
            moderation_1.pin(msg);
        }
        else if (comm.startsWith('art')) {
            var query = comm.replace('art', '').trim();
            article_1.findArticle(query, msg.channel);
        }
    }
    else if (cont.startsWith(_pre_admin) && msg.member.hasPermission("ADMINISTRATOR")) {
        var comm = cont.replace(_pre_admin, '');
        // Update store
        if (comm === 'update-store') {
            article_1.updateStore(msg.member.user);
        }
    }
});
bot.login(process.env.DISCORD_BOT_TOKEN)
    .catch(console.error);
// ******************************************
// Create a new express application instance
var app = express();
app.get('/', function (req, res) {
    res.send('Omnibus bot up!');
});
app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
//# sourceMappingURL=app.js.map