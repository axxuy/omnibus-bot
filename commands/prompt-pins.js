"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function promptPin(msg) {
    if (msg.content.includes('docs.google.com')) {
        var admin = msg.guild.roles.find(function (r) { return r.id === process.env.PROFESSOR_ROLE; });
        msg.pin()
            .catch(console.error);
        msg.channel.send("Hey, " + admin + ", a new submission has been pinned!");
    }
    else {
        msg.reply("You should include a GDocs (https://docs.google.com) link with your pin. It'll be easier to review it and make suggestions.")
            .catch(console.error);
    }
}
exports.promptPin = promptPin;
//# sourceMappingURL=prompt-pins.js.map