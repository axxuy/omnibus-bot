import {RichEmbed, TextChannel} from "discord.js";
import axios from "axios";


export function sendDerpi(chan: TextChannel, query: string, color: string) {
    axios.get(`https://derpibooru.org/search.json?q=${query}%2C+safe+score.gt:10&random_image=y`).then(function(r) {

        axios.get(`https://derpibooru.org/${r.data.id}.json`).then(function(r) {

            let embed: RichEmbed = new RichEmbed()
                .setTitle('🔗 Source')
                .setURL(`https://derpibooru.org/${r.data.id}`)
                .setColor(color)
                .setImage('https:'+r.data.image);
            chan.send(embed)
                .catch(console.error);

        })
        .catch(console.error);

    })
    .catch(console.error);
}
