import {Message} from "discord.js";

export function promptmeOn(msg: Message) {
    addRole(msg, process.env.PROMPTME_ROLE);
}

export function promptmeOff(msg: Message) {
    removeRole(msg, process.env.PROMPTME_ROLE);
}

export function discussionClubOn(msg: Message) {
    addRole(msg, process.env.DISCUSSION_CLUB_ROLE)
}

export function discussionClubOff(msg: Message) {
    removeRole(msg, process.env.DISCUSSION_CLUB_ROLE)
}



function addRole(msg: Message, role: string|undefined) {
    msg.member.addRole(role || '')
        .catch(console.error);
    msg.react('✅')
        .catch(console.error);
}

function removeRole(msg: Message, role: string|undefined) {
    msg.member.removeRole(role || '')
        .catch(console.error);
    msg.react('✅')
        .catch(console.error);
}
