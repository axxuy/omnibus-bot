import {Message, Role} from "discord.js";

export function promptPin(msg: Message) {
    if (msg.content.includes('docs.google.com')) {

        let admin: Role = msg.guild.roles.find(r => r.id === process.env.PROFESSOR_ROLE);

        msg.pin()
            .catch(console.error);

        msg.channel.send(`Hey, ${admin}, a new submission has been pinned!`);

    } else {

        msg.reply("You should include a GDocs (https://docs.google.com) link with your pin. It'll be easier to review it and make suggestions.")
            .catch(console.error);

    }
}

