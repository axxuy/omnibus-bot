import {Message, RichEmbed} from "discord.js";

export function help(msg: Message) {

    let embed: RichEmbed = new RichEmbed()
        .setColor('#3f43ff')
        .setTitle('Omnibus Bot Help')
        .addField(
            '!quote (!q)',
            'Show a random, writing-related quote'
        )
        .addField(
            '!sub (!s) [message]',
            'Used for pinning monthly prompt submissions. Has to contain a Gdoc link!'
        )
        .addField(
            '!promptme [on|off]',
            'Get or remove the `promptme` role'
        )
        .addField(
            '!discussion-club [on|off]',
            'Get or remove the `discussion club` role'
        );

    msg.channel.send(embed)
    .catch(console.error);
}

