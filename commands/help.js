"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var discord_js_1 = require("discord.js");
function help(msg) {
    var embed = new discord_js_1.RichEmbed()
        .setColor('#3f43ff')
        .setTitle('Omnibus Bot Help')
        .addField('!quote (!q)', 'Show a random, writing-related quote')
        .addField('!sub (!s) [message]', 'Used for pinning monthly prompt submissions. Has to contain a Gdoc link!')
        .addField('!promptme [on|off]', 'Get or remove the `promptme` role')
        .addField('!discussion-club [on|off]', 'Get or remove the `discussion club` role');
    msg.channel.send(embed)
        .catch(console.error);
}
exports.help = help;
//# sourceMappingURL=help.js.map