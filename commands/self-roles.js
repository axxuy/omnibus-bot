"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function promptmeOn(msg) {
    addRole(msg, process.env.PROMPTME_ROLE);
}
exports.promptmeOn = promptmeOn;
function promptmeOff(msg) {
    removeRole(msg, process.env.PROMPTME_ROLE);
}
exports.promptmeOff = promptmeOff;
function discussionClubOn(msg) {
    addRole(msg, process.env.DISCUSSION_CLUB_ROLE);
}
exports.discussionClubOn = discussionClubOn;
function discussionClubOff(msg) {
    removeRole(msg, process.env.DISCUSSION_CLUB_ROLE);
}
exports.discussionClubOff = discussionClubOff;
function addRole(msg, role) {
    msg.member.addRole(role || '')
        .catch(console.error);
    msg.react('✅')
        .catch(console.error);
}
function removeRole(msg, role) {
    msg.member.removeRole(role || '')
        .catch(console.error);
    msg.react('✅')
        .catch(console.error);
}
//# sourceMappingURL=self-roles.js.map