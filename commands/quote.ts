import {Message, RichEmbed, TextChannel} from "discord.js";
import fs = require('fs');

export function quote(msg: Message) {
    sendQuote(msg.channel as TextChannel);
}

export function sendQuote(chan: TextChannel) {
    fs.readFile('./quotes.json', function(err, data) {
        if (err) throw err;

        let quotes: Quote[] = JSON.parse(data.toString());
        let rand: number = Math.round(Math.random() * (quotes.length - 1));
        let q: Quote = quotes[rand];

        let embed: RichEmbed = new RichEmbed()
            .setColor('#008000')
            .setDescription(`*${q.Quote}*`)
            .setFooter(`~ ${q.Author}`);

        chan.send(embed)
            .catch(console.error);
    })
}

class Quote {
    public Quote: string;
    public Author: string;

    constructor(Quote: string, Author: string) {
        this.Quote = Quote;
        this.Author = Author;
    }
}
