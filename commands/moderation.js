"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function pin(msg) {
    if (process.env.HELPER_ROLE && msg.member.roles.find(function (r) { return r.id === process.env.HELPER_ROLE; })) {
        msg.delete()
            .catch(console.error);
        msg.channel.messages.find(function (m) { return m.member === msg.member; })
            .pin()
            .catch(console.error);
    }
}
exports.pin = pin;
//# sourceMappingURL=moderation.js.map