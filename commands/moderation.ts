import {Message} from "discord.js";

export function pin(msg: Message) {
    if (process.env.HELPER_ROLE && msg.member.roles.find(r => r.id === process.env.HELPER_ROLE)) {
        msg.delete()
            .catch(console.error);
        msg.channel.messages.find(m => m.member === msg.member)
            .pin()
            .catch(console.error);
    }
}
