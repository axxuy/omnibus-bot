"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var discord_js_1 = require("discord.js");
var fs = require("fs");
function quote(msg) {
    sendQuote(msg.channel);
}
exports.quote = quote;
function sendQuote(chan) {
    fs.readFile('./quotes.json', function (err, data) {
        if (err)
            throw err;
        var quotes = JSON.parse(data.toString());
        var rand = Math.round(Math.random() * (quotes.length - 1));
        var q = quotes[rand];
        var embed = new discord_js_1.RichEmbed()
            .setColor('#008000')
            .setDescription("*" + q.Quote + "*")
            .setFooter("~ " + q.Author);
        chan.send(embed)
            .catch(console.error);
    });
}
exports.sendQuote = sendQuote;
var Quote = /** @class */ (function () {
    function Quote(Quote, Author) {
        this.Quote = Quote;
        this.Author = Author;
    }
    return Quote;
}());
//# sourceMappingURL=quote.js.map