import {RichEmbed, TextChannel, User} from "discord.js";
import axios from "axios";
import Fuse = require("fuse.js");
import {FuseOptions} from "fuse.js";

const fs = require('fs');


export function findArticle(query: string, chan: TextChannel) {
    fs.readFile('./articlestore.json', (err: NodeJS.ErrnoException | null, data: Buffer) => {
        if (err) {

        } else {
            let articles: Article[] = JSON.parse(data.toString('utf8'));

            const options = {
                shouldSort: true,
                // includeScore: true,
                threshold: 0.6,
                location: 0,
                distance: 100,
                maxPatternLength: 32,
                minMatchCharLength: 1,
                keys: [
                    "title"
                ]
            };
            const fuse = new Fuse(articles, options);
            let result: Article[] = fuse.search(query);

            let embed = new RichEmbed()
                .setTitle("Here's your results!")
                .setFooter("Find all at https://sfnw.site");
            result
                .splice(-5)
                .reverse()
                .forEach(e => {
                    embed.addField(e.title, `By ${e.author} – [🔗 Read](https://sfnw.site/${e.url})`)
                });

            chan.send(embed)
                .catch(console.error);
        }
    })
}

export function updateStore(user: User) {
    axios.get('https://sfnw.site/api.json').then(r => {
        fs.writeFile('./articlestore.json', JSON.stringify(r.data), (err: NodeJS.ErrnoException | null) => {
            if (err) {
                user.createDM().then(r => {
                    r.send(`**An error has occurred:**\n\`\`\`json\n${err}\n\`\`\``)
                })
            } else {
                user.createDM().then(r => {
                    r.send(`Store updated successfully!`)
                })
            }
        })
    })
}

interface Article {
    author: string,
    category: string,
    date: string,
    tags: string[],
    title: string,
    url: string
}
