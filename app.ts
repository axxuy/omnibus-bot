import express = require('express');
import discord = require("discord.js");
import cron = require("node-schedule");

import {Client, Message, TextChannel} from "discord.js";

import {help} from "./commands/help";
import {quote, sendQuote} from "./commands/quote";
import {promptmeOn, promptmeOff, discussionClubOn, discussionClubOff} from "./commands/self-roles";
import {pin} from "./commands/moderation";
import {promptPin} from "./commands/prompt-pins";
import {sendDerpi} from "./commands/derpi";
import {findArticle, updateStore} from "./commands/article";



// Consts
const bot: Client = new discord.Client();
// Prefix
const _pre: string = '!';
const _pre_admin: string = '&';
// Schedules
const schedules: { t4: string; t3b: string; t6: string; t3a: string; t8: string; t2: string; tt: string } = {
    t2:  "0 0,12 * * *",
    t3a: "0 0,8,16 * * *",
    t3b: "0 4,12,20 * * *",
    t4:  "0 0,6,12,18 * * *",
    t6:  "0 0,4,8,12,16,20 * * *",
    t8:  "0 0,3,6,9,12,15,18,21 * * *",
    tt:  "* * * * *",
};

// Bot proper
bot.on('ready', () => {
    console.log('Bot is ready!');
});

// Schedule
let q = cron.scheduleJob(schedules.t4, () => {
    let chan: TextChannel = bot.channels.get(process.env.OFFTOPIC_CHANNEL || '') as TextChannel;
    sendQuote(chan);
});

let q2 = cron.scheduleJob(schedules.t3a, () => {
    let chan: TextChannel = bot.channels.get(process.env.PICS_CHANNEL || '') as TextChannel;
    sendDerpi(chan, 'rarijack', '#ff9e18')
});

let q3 = cron.scheduleJob(schedules.t3b, () => {
    let chan: TextChannel = bot.channels.get(process.env.PICS_CHANNEL || '') as TextChannel;
    sendDerpi(chan, 'startrix', '#b454ff')
});

// Handle commands
bot.on('message', (msg: Message) => {
    const cont: string = msg.content;

    if (cont.startsWith(_pre)) {
        let comm: string = cont.replace(_pre, '');

        // Help command
        if (comm === 'h' || comm === 'help') {
            help(msg);
        }

        // Quote command
        else if (comm === 'q' || comm === 'quote') {
            quote(msg);
        }

        // Role management – promptme
        else if (comm.startsWith('promptme')) {
            let m = comm.replace('promptme', '').trim();
            if (m === 'on')
                promptmeOn(msg);
            else if (m === 'off')
                promptmeOff(msg);

        // Role management – discussion club
        } else if (comm.startsWith('discussion-club')) {
            let m = comm.replace('discussion-club', '').trim();
            if (m === 'on')
                discussionClubOn(msg);
            else if (m === 'off')
                discussionClubOff(msg);
        }

        // Prompt submission pinning
        else if (comm.startsWith('sub')) {
            promptPin(msg);
        }

        // Helpers' pin
        else if (comm === 'pin') {
            pin(msg);
        }

        else if (comm.startsWith('art')) {
            let query = comm.replace('art', '').trim();
            findArticle(query, msg.channel as TextChannel);
        }

    } else if (cont.startsWith(_pre_admin) && msg.member.hasPermission("ADMINISTRATOR")) {

        let comm: string = cont.replace(_pre_admin, '');

        // Update store
        if (comm === 'update-store') {
            updateStore(msg.member.user);
        }

    }
});

bot.login(process.env.DISCORD_BOT_TOKEN)
    .catch(console.error);



// ******************************************
// Create a new express application instance
const app: express.Application = express();

app.get('/', function (req, res) {
    res.send('Omnibus bot up!');
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
